import './App.css';
import { Form, Field } from 'react-final-form';
import React from 'react';

function onSubmit (values) {
  window.alert(JSON.stringify(values, 0, 2));
};

const mustNotContainNumbers = (value) => (/^[a-z\s]+$/i.test(value) ? undefined : "Letters only");
const mustBeNumber = (value) => (value && !isNaN(value) ? undefined : "Must be a number");
const maxLength = (min) => (value) => 
  value && value.length >= min ? `Should be shorter than ${min} symbols` : undefined;

function MyForm() {
  return (
  <Form
    onSubmit={onSubmit}
    initialValues={{stooge: 'Larry'}}
    render={({ handleSubmit, form, submitting, pristine, values }) => (
      <form onSubmit={handleSubmit}>
        <Field name="firstName" validate={mustNotContainNumbers}>
          {({ input, meta }) => (
            <div>
              <label>First Name</label>
              <input {...input} type="text" placeholder="First Name" className='firstName' />
              {meta.error && !meta.pristine && <span>{meta.error}</span>}
              {meta.error && !meta.pristine && <style>{document.querySelector('.firstName').classList.add('invalid')}</style>}
              
            </div>
          )}
        </Field>

        <Field name="lastName" validate={mustNotContainNumbers}>
          {({ input, meta }) => (
            <div>
              <label>First Name</label>
              <input {...input} type="text" placeholder="Last Name" className='lastName' />
              {meta.error && !meta.pristine && <span>{meta.error}</span>}
              {meta.error && !meta.pristine && <style>{document.querySelector('.lastName').classList.add('invalid')}</style>}
            </div>
          )}
        </Field>

        <Field name="age" validate={mustBeNumber}>
          {({ input, meta }) => (
            <div>
              <label>First Name</label>
              <input {...input} type="text" placeholder="Age" className='age' />
              {meta.error && !meta.pristine && <span>{meta.error}</span>}
              {meta.error && !meta.pristine && <style>{document.querySelector('.age').classList.add('invalid')}</style>}
            </div>
          )}
        </Field>

        <div>
          <label>Employed</label>
          <Field name="employed" component="input" type="checkbox" />
        </div>

        <div>
          <label>Favorite Color</label>
          <Field name="favoriteColor" component="select">
            <option />
            <option value="#ff0000">Red</option>
            <option value="#00ff00">Green</option>
            <option value="#0000ff">Blue</option>
          </Field>
        </div>

        <div>
          <label>Sauces</label>
          <div className='checkboxBlock'>
            <label>
              <Field name="sauces" component="input" type="checkbox" value="ketchup" />{" "}Ketchup
            </label>
            <label>
              <Field name="sauces" component="input" type="checkbox" value="mustard" />{" "}Mustard
            </label>
            <label>
              <Field name="sauces" component="input" type="checkbox" value="mayonnaise" />{" "}Mayonnaise
            </label>
            <label>
              <Field name="sauces" component="input" type="checkbox" value="guacamole" />{" "}Guacamole
            </label>
          </div>
        </div>

        <div>
          <label>Best Stooge</label>
          <div className='radioBlock'>
            <label>
              <Field name="stooge" component="input" type="radio" value="larry" checked={true} />{" "}Larry
            </label>
            <label>
              <Field name="stooge" component="input" type="radio" value="moe" />{" "}Moe
            </label>
            <label>
              <Field name="stooge" component="input" type="radio" value="curly" />{" "}Curly
            </label>
          </div>
        </div>

        <Field name="notes" validate={maxLength(100)}>
          {({ input, meta }) => (
            <div>
              <label>Notes</label>
              <input {...input} type="textarea" placeholder="Notes" className='notes' />
              {meta.error && !meta.pristine && <span>{meta.error}</span>}
              {meta.error && !meta.pristine && <style>{document.querySelector('.notes').classList.add('invalid')}</style>}
            </div>
          )}
        </Field>

        <div>
          <button type="submit" disabled={pristine}>Submit</button>
          <button type="reset" disabled={pristine} onClick={() => {
            form.reset()
            if (document.querySelector('.invalid')) document.querySelector('.invalid').classList.remove('invalid');
          }
            }>Reset</button>
        </div>

        <pre>{JSON.stringify(values, 0, 2)}</pre>

      </form>
    )}
  />
)
    }

function App() {
  return (
    <div className="App">
      <MyForm />
    </div>
  );
}

export default App;
